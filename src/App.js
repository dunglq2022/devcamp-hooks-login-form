import { Container, ButtonGroup, Button, Grid } from "@mui/material";
import { grey } from "@mui/material/colors";
import { useState } from "react";
import Login from "./components/Login";
import Signup from "./components/Signup";

function App() {
  const [isLogin, setIsLogin] = useState(true);
  return(
    <Container maxWidth={'sm'} sx={{
      mt:4,
      display: 'flex',
      justifyContent: 'center',
    }}>
      <Grid sx={{
          backgroundColor: grey[100],
          height: '60vh'          
        }}>
        <ButtonGroup sx={{
          width:'100%',
        }} variant="contained" aria-label="outlined primary button group">
          <Button fullWidth color= {isLogin ? 'inherit' : 'success'} onClick={() => setIsLogin(false)}>Singup</Button>
          <Button fullWidth color= {isLogin ? 'success' : 'inherit'} onClick={() => setIsLogin(true)}>Login</Button>
        </ButtonGroup>
        {isLogin ? <Login/> : <Signup/>}
      </Grid>
    </Container>
  )
}

export default App;
