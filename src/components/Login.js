import { Button, Container, Grid, TextField, Typography } from "@mui/material";
import { green } from "@mui/material/colors";

function Login () {
    return(
        <Container>
            <Grid container>
                <Grid item lg={12} md={12} sm={12} xs={12}> 
                <Typography 
                variant="h4"
                sx={{
                    textAlign: "center",
                    mt: 2
                }}
                >
                    {'Welcome Back'}
                </Typography>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                <TextField sx={{
                    mt: 2
                }} label={'Email Address'} type="email" fullWidth />
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                <TextField sx={{
                    mt: 2
                }} label={'Password'} type="password" fullWidth />
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                <Typography sx={{
                    display: 'flex',
                    justifyContent:'end' ,
                    fontSize: 12,
                    mt: 1,
                    color: green[900]
                }}>
                    {'Forgot Password?'}
                </Typography>
                </Grid>
                <Button sx={{
                    mt: 3,
                    fontSize: 20
                }} variant="contained" color="success" fullWidth>
                    LOG IN
                </Button>
            </Grid>            
        </Container>
    )
}
export default Login;