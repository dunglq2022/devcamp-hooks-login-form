import { Button, Container, TextField, Typography, Grid } from "@mui/material";

function Signup () {
    return(
        <Container>
            <Grid container>
                <Grid item lg={12} md={12} sm={12} xs={12}> 
                <Typography 
                variant="h4"
                sx={{
                    textAlign: "center",
                    mt: 2
                }}
                >
                    {'Welcome Back'}
                </Typography>
                </Grid>
                <Grid sx={{
                    paddingRight: 0.5,
                    mt: 2
                }} item lg={6} md={6} sm={12} xs={12}>
                <TextField id="first-name" label={'First Name'} type="text" fullWidth/>
                </Grid>
                <Grid sx={{
                    mt: 2,
                    paddingLeft: 0.5
                }} item lg={6} md={6} sm={12} xs={12}>
                <TextField id="last-name" label={'Last Name'} type="text" fullWidth />
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                <TextField sx={{
                    mt: 2
                }}id="email-address" label={'Email Adress'} type="text" fullWidth />
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                <TextField sx={{
                    mt: 2
                }} label={'Set A Password'} type="password" fullWidth />
                </Grid>
                <Button sx={{
                    mt: 3,
                    fontSize: 20
                }} variant="contained" color="success" fullWidth>
                    {'GET STARTED'}
                </Button>
            </Grid>            
        </Container>
    )
}
export default Signup;